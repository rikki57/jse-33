package ru.nlmk.study.jse33.cfuture;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class WithCompletableFuture {
    public static void main(String[] args) throws InterruptedException {
        Set<Path> inputs = getPathsForDirectory(".\\resources\\testset");
        final List<Integer> results = new ArrayList<>();
        for (Path p : inputs) {
            CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
                Integer threadSum = 0;
                try {
                    Thread.sleep(4000);
                    threadSum = IntStream.of(Files.lines(p).mapToInt(Integer::parseInt).toArray()).sum();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
                return threadSum;
            });
            future.thenAccept(result -> {
                synchronized (results) {
                    results.add(result);
                }
            });
        }
        int i = 0;
        while (results.size() < inputs.size()) {
            System.out.println("Iteration" + i++);
            Thread.sleep(500);
        }
        System.out.println("Result = " + getSum(results));

    }

    private static Set<Path> getPathsForDirectory(String directory) {
        Set<Path> paths = new HashSet<>();
        try (Stream<Path> walk = Files.walk(Paths.get(directory))) {
            paths = walk.filter(Files::isRegularFile).collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }

    private static Integer getSum(List<Integer> input) {
        return IntStream.of(input.stream().mapToInt(Number::intValue).toArray()).sum();
    }

}
