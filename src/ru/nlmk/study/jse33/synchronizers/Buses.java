package ru.nlmk.study.jse33.synchronizers;

import java.util.concurrent.Exchanger;

public class Buses {
    private static final Exchanger<String> EXCHANGER = new Exchanger<>();

    public static void main(String[] args) throws InterruptedException {
        String[] p1= new String[] {"Пассажир A->D", "Пассажир A->C"};
        String[] p2= new String[] {"Пассажир В->С", "Пассажир B->D"};
        new Thread(new Bus(1, "A", "D", p1)).start();
        Thread.sleep(200);
        new Thread(new Bus(2, "B", "C", p2)).start();
    }

    public static class Bus implements Runnable{
        private int number;
        private String from;
        private String to;
        private String[] passengers;

        public Bus(int number, String from, String to, String[] passengers) {
            this.number = number;
            this.from = from;
            this.to = to;
            this.passengers = passengers;
        }

        @Override
        public void run() {
            try {
                System.out.printf("В автобус №%d сели пассажиры %s, %s \n", number, passengers[0], passengers[1]);
                System.out.printf("Автобус №%d едет из пункта %s в пункт %s \n", number, from, to);
                Thread.sleep(1000 + (long) Math.random() * 5000);
                System.out.printf("Bus number  №%d arrived in point exchange \n", number);
                passengers[1] = EXCHANGER.exchange(passengers[1]);
                System.out.printf("Bus  №%d moved passenger for %s \n", number, to);
                Thread.sleep(1000 + (long) Math.random() * 5000);
                System.out.printf("автобус №%d came to %s with %s, %s \n", number, to, passengers[0], passengers[1]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
