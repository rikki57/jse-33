package ru.nlmk.study.jse33.synchronizers;

import java.util.concurrent.Semaphore;

public class Bus {
    private static final boolean[] SEATS = new boolean[10];
    private static final Semaphore SEMAPHORE = new Semaphore(10, true);

    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 20; i++) {
            new Thread(new Passenger(i)).start();
            Thread.sleep(400);
        }
    }

    public static class Passenger implements Runnable {
        private int paxNumber;

        public Passenger(int paxNumber) {
            this.paxNumber = paxNumber;
        }

        @Override
        public void run() {
            System.out.printf("Пассажир №%d пытается зайти в автобус.\n", paxNumber);
            try {
                SEMAPHORE.acquire();
                int seatNumber = -1;
                synchronized (SEATS) {
                    for (int i = 0; i < 10; i++)
                        if (!SEATS[i]) {      //Если место свободно
                            SEATS[i] = true;  //занимаем его
                            seatNumber = i;         //Наличие свободного места, гарантирует семафор
                            System.out.printf("Пассажир №%d сел на место %d.\n", paxNumber, i);
                            break;
                        }
                }
                Thread.sleep(5000);
                synchronized (SEATS) {
                    SEATS[seatNumber] = false;//Освобождаем место
                }
                SEMAPHORE.release();
                System.out.printf("Пассажир №%d вышел из автобуса.\n", paxNumber);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
