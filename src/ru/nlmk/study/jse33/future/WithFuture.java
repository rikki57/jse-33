package ru.nlmk.study.jse33.future;

import ru.nlmk.study.jse33.simple.Counter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class WithFuture {
    public static void main(String[] args) throws InterruptedException {
        Set<Path> inputs = getPathsForDirectory(".\\resources\\testset");
        Integer sum = 0;
        ExecutorService executorService = Executors.newCachedThreadPool();
        List<Callable<Integer>> tasks = new ArrayList<>();
        for (Path p : inputs) {
            tasks.add(() -> {
                Integer threadSum = 0;
                try {
                    threadSum = IntStream.of(Files.lines(p).mapToInt(Integer::parseInt).toArray()).sum();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return threadSum;
            });
        }
        List<Future<Integer>> results = executorService.invokeAll(tasks);
        for (Future<Integer> result : results) {
            try {
                sum += result.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println(sum);
    }

    private static Set<Path> getPathsForDirectory(String directory) {
        Set<Path> paths = new HashSet<>();
        try (Stream<Path> walk = Files.walk(Paths.get(directory))) {
            paths = walk.filter(Files::isRegularFile).collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }

    private static Integer getSum(List<Integer> input) {
        return IntStream.of(input.stream().mapToInt(Number::intValue).toArray()).sum();
    }
}
