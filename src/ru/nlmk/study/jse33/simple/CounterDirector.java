package ru.nlmk.study.jse33.simple;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class CounterDirector {
    public Integer count(Integer n, Integer threadCount){
        Integer interval = n / threadCount;
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            makeCounter(interval * i, interval * (i + 1) - 1, threads);
        }
        makeCounter(interval * threadCount, n, threads);
        Integer result = 0;
        for (Thread thread : threads){
            try {
                thread.join();
                result += ((Counter)thread).getSum();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private void makeCounter(int start, int end, List<Thread> threads) {
        Integer sum = 0;
        Counter counter = new Counter(start, end, sum);
        threads.add(counter);
        counter.start();
    }
}
