package ru.nlmk.study.jse33.simple;

public class Counter extends Thread {
    private Integer first;
    private Integer last;
    private Integer sum;

    public Counter(Integer first, Integer last, Integer sum) {
        this.first = first;
        this.last = last;
        this.sum = sum;
    }

    @Override
    public void run() {
        System.out.println(currentThread().getName() + " Start = " + first + "; Last = " + last);
        for (int i = first; i <= last; i++){
            sum += i;
        }
        System.out.println(currentThread().getName() + sum);
    }

    public Integer getSum() {
        return sum;
    }
}
